#import "@preview/unequivocal-ams:0.1.0": ams-article, theorem, proof, 

#set page(margin: 1.75in)
#set par(leading: 0.55em, first-line-indent: 1.8em, justify: true)
#set text(font: "New Computer Modern")
#show raw: set text(font: "New Computer Modern Mono")
#show par: set block(spacing: 0.55em)
#show heading: set block(above: 1.4em, below: 1em)

#show: ams-article.with(
  title: [Lebesgue Measurability of subsets of $RR$ under the axiom of determinancy],
  authors: (
    ( name: "Fadri Lardon" ),
  ),
  abstract: "We show that the axiom of determinancy implies Lebesgue Measurability for all subsets of the real numbers",
  bibliography: none,
)

#let lemma(body, numbered: true) = figure(
  body,
  kind: "theorem",
  supplement: [Lemma],
  numbering: if numbered { "1" },
)


#let bow = box(baseline: 0.3em,math.paren.t)

*Axiom of determinacy*: 

    every countable combinatiorial game is determined (i.e. has a winning strategy)

#linebreak(justify:true) 
We are going to need the following lemma proven in #cite(<Book>) : 
#lemma[under AD we have choice for a countable number of subsets of $RR$]

This is mostly a reformulation of #cite(<Martin2003>) so all credit goes to that paper and its author.

= the measure
To start we conceptualize the unit interval $[0,1]$ as $2^(omega)$ and our desired measure $mu : 2^omega -> [0,1]$ 
as the coin-flipping measure on that. Meaning the measure such that 
for $p$ in $2^(<omega)$ we have  $mu({x in 2^omega : x|_(|p|) = p}) = 2^(-|p|)$ when identifying 
elements from $2^omega$ with binary fractions; we get a surjective map $2^omega ->> [0,1]$.
The Lebesgue measure agrees with $mu$ on all sets measurable by its premeasure hence by 
the uniqueness of the Carethéodory-Hahn extension implies that all sets measurable by $mu$
are Lebesgue measurable and that $mu$ coincides with the Lebesgue-measure on those, which is enough here.
Let $mu_*$ denote the inner- and $mu^*$ the outer measure associated with $mu$.

= the game
In the following let $J := [0,1] sect QQ$. For a fixed $cal(A) subset.eq 2^omega$
for each value \ $v in (0,1]$ define the game $G_v$ in the following way:
Let $v_0 = v$ and at each turn $i$ player I chooses $h_i colon 2 -> J$ such that $
(h_i (0) + h_i (1))/2 >= v_(i-1)
$.
Player II responds by choosing $e_i in 2$ such that $h_i (e_i) != 0$. Now let $v_i := h_i (e_i)$
and have the game continue.


#align(center)[
#box(inset: 0.5em, $
        &"I " &h_1 &&h_2 && h_3 && h_4 && h_5   &&...   \
        &"II" &&e_1 && e_2 && e_3 && e_4 && e_5 &&...
    $
)
]

For any position $x = h_1 e_1 h_2 e_2 ... $ of the game call $
pi(x) = e_1 e_2 e_3 ... in 2^(<omega)
$.
For any play $x^*$ of  $G_v$ let 
$
pi(x^*) = union.big_(x lt.eq.curly x*) pi(x) in 2^omega
$.
Player I wins if $pi(x^*) in cal(A)$ and player II wins if $pi(x^*) in.not cal(A)$.


= the proof
The idea of the game is the following: 
- I is trying to prove that $mu_*(cal(A)) >= v$
- II is trying to prove I wrong.

#lemma()[If _I_ has a winning strategy for $G_v$ then $mu_*(cal(A)) >= v$]

#proof[
    Let $sigma$ be a winning strategy for I and $p in 2^(<omega)$
    We want to inductively define a notion of acceptability on $2^(<omega)$.
    When we call $p in 2^(<omega)$ acceptable we associate with it a play $psi(p)$ consistent with $sigma$
    such that $pi(psi(p)) = p$ and $|psi(p)| = 2 |p|$. 
    Any $q lt.eq.curly p$ will also be called acceptable with $psi(q) lt.eq.curly psi(p)$. To start call $emptyset$ acceptable with $psi(emptyset) = emptyset$.
    For $p$ acceptable let $h^p = sigma(p) : 2 -> J$. For $e in 2$  we shall call $p bow e$ acceptable 
    if $h^p (e) != 0$ and set $psi(p bow e) = psi(p) bow h^p bow e$.
    We call $x in 2^omega$ acceptable if for any $p lt.curly x$ $p$ is acceptable

    For $p$ acceptable set $v^p = v_(|p|)$ in $psi(p)$ and set 
    $
        f(p) := cases(v^p &"if " p "is acceptable", 0 &"otherwise")
    $
    
    In particular if $p$ is acceptable then $f(p bow e) = h^p (e)$ since $p bow e$ is unacceptable 
    only if $h^p (e) = 0$. Now we can see that fir $p$ acceptable
    $
        (f(p bow 0) + f(p bow 1))/2 = (h^p (0) + h^p (1)) /2 >= v_(|p|) = f(p)
    $ .
    And since the same inequality trivially holds for unacceptable $p$, we get 
    $
        ( f(p bow 0) + f (p bow 1)) / 2 >= f(p)
    $
    for all $p in 2^(<omega)$
    
    Thus for any $n in NN$
    $
        sum_(p in 2^(<omega) \ |p| = n) 2^(-n) f(p) >= v
    $   

    now since $f(p) <= 1$ and $f(p) = 0$ for $p$ unacceptable we have
    $
       mu({x | x|_n "is acceptable"}) &= sum_(p in 2^(<omega) \ |p| = n\ p "acceptable") mu({x | x|_n = p}) \
                                      &= sum_(p in 2^(<omega) \ |p| = n\ p "acceptable") 2^(-n) \
                                      &>= sum_(p in 2^(<omega) \ |p| = n) 2^(-n) f(p) \ &>= v
    $.

    Now the set $C subset.eq cal(A)$ of all acceptable $p$ is a measurable subset of $cal(A)$
    , since $
        C = sect.big_(n in omega) union.big_(p in 2^(<omega) \ |p| = n \ p "acceptable") {w in 2^omega : p lt.eq.curly w}
    $
    and ${w in 2^omega : w|_n = p}$ are measurable. We thus conclude that
    $
        mu_*(cal(A)) >= mu(C) >= v 
    $   
    
]

#lemma()[If _II_ has a winning strategy for $G_v$ then $mu^*(cal(A)) <= v$]

#proof[
    Let $tau$ be a winning strategy for II and $delta in [0, 1]$ arbitrary.
    We once more want to inductively define a notion of acceptability on $2^(<omega)$
    When we call $p in 2^(<omega)$ acceptable we once more associate with it a $psi(p)$ in $G_v$
    consistent with $tau$, meeting $pi(psi(p)) = p$, fulfilling $|psi(p)| = 2 |p|$
    and such that for $q lt.eq.curly p$ we get $psi(q) lt.eq.curly  psi(p)$.
    We once again call $x in 2^omega$ acceptable if for all $p lt.curly x$ we have that $p$ is acceptable.
    
    We call $emptyset$ acceptable with $psi(emptyset) = emptyset$ and for $p$ acceptable define
    $v^p := v_(|p|)$ in $psi(p)$. And for $e in 2$ let $
        u^p (e) := inf ({h(e) | h "legal at" psi(p) "and with" tau(psi(p) bow h) = e} union {1})
    $ 
    We show that $
        (u^p (0) + u^p (1))/ 2 <= v^p 
    $
    
    #proof[
        Since otherwise $exists epsilon > 0$ such that
        $
            (u^p (0) - epsilon + u^p (1) - epsilon) / 2 >= v^p
        $
        which would make $h(e) := u^p (e) - epsilon$ a legal choice for player I. 
        This contradicts the definition of $u^p$ #math.arrow.zigzag.
    ]

    Now for $e in 2$ we shall call $p bow e$ acceptable if $u^p (e) < 1$. In that case define \ $h^(p,e)colon 2 -> J$
    such that it is legal at $psi(p)$,
    $
        h^(p, e) (e) <= u^p (e) + 2^(- |p| - 1) delta 
    $
    and $tau(psi(p) bow h^(p, e)) = e$. We can make this choice since according to Lemma 0.1 (Lemma 16.2 in
    #cite(<Book>)).
    the axiom of determinancy allows us to make choices for a countable number of subsets of $RR$.

    
    For $p bow e$ not acceptable set $h^(p, e) equiv 1$. Note that $h^(p,e)(e) <= u^p (e) + 2^(-|p|-1) delta$
    holds still and thus for all acceptable $p$.
    Now define $f: 2^(<omega) -> (0,1]$ by 
    $
        f(p) = cases(v^p &"if " p "is acceptable", 1 &"otherwise")
    $

    Now for $p$ acceptable and $|p| = n$ we have
    $
        (f(p bow 0) + f(p bow 1)) / 2 &= (h^(p, 0)(0) + h^(p, 1)(1))/2 \
                               &<= (u^p (0) + u^p (1))/ 2 + 2^(-|p|-1)delta \
                               &<= v^p + 2^(-|p|-1)delta \
                                &= f(p) + 2^(-|p|-1)delta  
    $.
    
    Note that this inequality also holds trivially if $p$ is unacceptable.

    Hence we get for $n in NN$, that
    $
        sum_(p in 2^(<omega)\ |p| = n)2^(-n) f(p) <= v + (2^n - 1)/(2^n) delta
    $   
    
    Now, since $f(p) >= 0$ for all $p in 2^(<omega)$ and $f(p) = 1$ for $p$ unacceptable, we get that for $n in NN$
    $
        mu({x | x|_n "is unacceptable"}) <= v + (2^n - 1)/2^n delta
    $ 

    The set $C$ of all acceptable states is a measurable subset of $cal(A)^c$.
    Hence 
    $
        mu(C) <=  1 - v  - delta => mu^*(cal(A)) <= v + delta
    $

    and since $delta$ was arbitrary we get 
    $
        mu^*(cal(A)) <= v
    $



]

Now to conclude we use the axiom of determinancy to see that any $G_v$ is determined (since $J$ is countable  we can do that).
By contrapositive of the two lemmata we get that if I has winning strategy for $w$ then I has winning strategy for any $v < w$ 
And by same argument if  II has a winning strategy for w then they also do for any $v > w$


Now let $
alpha = sup{v in [0,1] : "I has a winning strategy for" G_v }
$
By the previous assertion we get $
    mu_*(cal(A)) >= alpha "since" mu_*(cal(A)) >= alpha - epsilon  " for " epsilon > 0
$
and 
$
    mu^*(cal(A)) <= alpha "since" mu^*(cal(A)) <= alpha + epsilon  " for " epsilon > 0
$

but now 
$
    alpha <= mu_*(cal(A)) <= mu^*(cal(A)) <= alpha
$

and hence $cal(A)$ is Lebesgue-measurable and 
$
    mu(cal(A)) = sup{v in [0,1] : "I has a winning strategy for" G_v }.
$
#pagebreak()

#bibliography("source.bib")
